import mqtt_sql
import web

if __name__ == "__main__":
    mqttc = mqtt_sql.init_mqtt()
    web.app.run(host='0.0.0.0', port=8080)
