# -*- coding: utf-8 -*-

import paho.mqtt.client as mqtt
import sqlite3
import datetime
import timeit


def unix_time_millis(dt):
    epoch = datetime.datetime.utcfromtimestamp(0)
    return (dt - epoch).total_seconds() * 1000.0


# Função executada ao iniciar conexão mqtt
def on_connect(client, userdata, flags, rc):
    print("Cliente MQTT conectado com status {}".format(rc))

    # Aqui o '#' indica wildcard; inscreve-se em todos os tópicos começados por /smartgrid/update/
    client.subscribe('/smartgrid/update/#')


# Função executada ao receber mensagem
# Formato do tópico: /smartgrid/update/<dispositivo>/<parâmetro>/<timestamp, em epoch Unix>
# Ex.: /smartgrid/update/computador-1/corrente/1487631650
# O payload da mensagem MQTT representa o valor
def on_message(client, userdata, message):
    info = message.topic.split('/')         # Dividir o topic da mensagem MQTT
    device = info[3]                        # Pegar o nome do dispositivo
    parameter = info[4]                     # Pegar o parâmetro recebido (tensão, corrente, etc)
    #timestamp = info[5]                     # Pegar a estampa de tempo
    # TODO: mudar de volta para timestamp adquirida do tópico
    timestamp = unix_time_millis(datetime.datetime.now())
    value = message.payload                 # Pegar o valor recebido

    # Inserir dados no banco de dados
    start = timeit.default_timer()  # Medição de performance (travamento do bd)

    # Guardar no banco de dados a mensagem recebida
    sql_conn = sqlite3.connect('data.db', timeout=5)
    c = sql_conn.cursor()
    c.execute('INSERT INTO DataPoint (device, parameter, value, datetime) VALUES (?, ?, ?, ?)',
              (device, parameter, value, timestamp))
    sql_conn.commit()
    sql_conn.close()

    total_time = timeit.default_timer() - start
    total_time *= 1000  # Converter para ms

    # print('{} :: {} :: {} (db lock: {:.2f} ms)'.format(device, parameter, value, total_time))


def init_mqtt():
    # Inicializar a instância de cliente MQTT
    mqttc = mqtt.Client()

    # Setar callbacks
    mqttc.on_connect = on_connect
    mqttc.on_message = on_message

    # Conectar e iniciar thread que escuta MQTT (não bloqueia)
    mqttc.connect('localhost')
    mqttc.loop_start()

    return mqttc


def exit_mqttc(mqttc):
    # Desconectar thread MQTT
    mqttc.loop_stop()
    mqttc.disconnect()




