# -*- encoding: utf-8 -*-

import flask
import json
import sqlite3

app = flask.Flask(__name__)


# Função que retorna a página principal, que mostra o gráfico
@app.route("/")
def chart():
    f = open('web/chart.html', 'r')
    page = f.read()
    f.close()
    return page


# Função que pega os pontos mais recentes, para atualização do gráfico
@app.route("/update", methods=['GET'])
def get_point():

    # A requisição traz um parâmetro GET device, que indica o dispositivo sobre os qual devemos fornecer os dados
    device_name = flask.request.args.get('device', '')

    sql_conn = sqlite3.connect('data.db', timeout=5)
    c = sql_conn.cursor()
    c.execute('SELECT * FROM DataPoint WHERE device = ? ORDER BY datetime DESC LIMIT 2', (device_name,))

    data = c.fetchall()
    to_return_v = None
    to_return_i = None

    # print (data)

    for j in xrange(0, len(data)):
        if data[j][2] == 'tensao':
            to_return_v = [data[j][4], data[j][3]]
        elif data[j][2] == 'corrente':
            to_return_i = [data[j][4], data[j][3]]

    sql_conn.close()

    return json.dumps([to_return_v, to_return_i])


#Função que pega os pontos iniciais para inicialização dos dados do gráfico
@app.route("/get_json", methods=['GET'])
def get_json():

    # A requisição traz um parâmetro GET device, que indica o dispositivo sobre os qual devemos fornecer os dados
    device_name = flask.request.args.get('device', '')

    sql_conn = sqlite3.connect('data.db', timeout=5)
    c = sql_conn.cursor()
    c.execute('SELECT * FROM DataPoint WHERE device = ? ORDER BY datetime DESC LIMIT 5000', (device_name,))

    data = c.fetchall()
    sql_conn.close()

    to_return_v = []
    to_return_i = []

    for j in xrange(0, len(data)):
        if data[j][2] == 'tensao':
            to_return_v.append([data[j][4], data[j][3]])
        elif data[j][2] == 'corrente':
            to_return_i.append([data[j][4], data[j][3]])


    to_return_v.reverse()
    to_return_i.reverse()
    return json.dumps([to_return_v, to_return_i, [device_name]])


# Pegar lista de dispositivos presentes no banco de dados
@app.route("/get_device_list")
def get_device_list():

    sql_conn = sqlite3.connect('data.db', timeout=5)
    c = sql_conn.cursor()
    c.execute('SELECT DISTINCT device FROM DataPoint;')

    device_list = c.fetchall()
    sql_conn.close()

    return json.dumps(device_list)

